version "4.10"

class DayNightTracker : Thinker
{
	static DayNightTracker Get()
	{
		let it = ThinkerIterator.Create('DayNightTracker', STAT_STATIC);
		let t = DayNightTracker(it.Next());
		if (!t)
		{
			t = new('DayNightTracker');
			t.ChangeStatNum(STAT_STATIC);
		}
		return t;
	}

	clearscope double GetTimeFactor()
	{
		return TimeTicks / double(DayNightHandler.GetTicksPerDay());
	}

	clearscope double GetLightFactor()
	{
		double fac = GetTimeFactor();
		double sunrise = DayNightHandler.SmoothStep(fac, 0.15, 0.45);
		double sunset = 1.0 - DayNightHandler.SmoothStep(fac, 0.65, 0.95);

		return fac <= 0.5 ? sunrise : sunset;
	}

	void AdvanceTime(int ticks)
	{
		TimeTicks += ticks;
		TimeTicks %= DayNightHandler.GetTicksPerDay();
	}

	bool HasBeenSet;
	int TimeTicks;
}

class DayNightHandler : EventHandler
{
	static clearscope double SmoothStep(double x, double edge0, double edge1)
	{
		x = clamp((x - edge0) / (edge1 - edge0), 0.0, 1.0);
		return x * x * (3 - x * 2);
	}

	static clearscope int GetTicksPerDay()
	{
		return dnc_cycletime > 0 ? dnc_cycletime * 60 * TICRATE : 24 * 60 * 60 * TICRATE;
	}

	// [Ace] Really wish I had DSCore here...
	static double, int GetSectorArea(Sector s)
	{
		double secSize = 0;
		for (int i = 0; i < s.lines.Size(); ++i)
		{
			Line ln = s.lines[i];
			secSize += ln.delta.Length();
		}

		return secSize, s.lines.Size();
	}

	override void UiTick()
	{
		if (Disabled || consoleplayer != Net_Arbitrator)
		{
			return;
		}

		if (Level.time % 35 == 0 && Tracker)
		{
			EventHandler.SendNetworkEvent("dnc_settime:"..SystemTime.Format("%H-%M-%S", SystemTime.Now()));
		}
	}

	override void WorldLoaded(WorldEvent e)
	{
		let it = ThinkerIterator.Create('SkyViewpoint');
		Actor viewer = null;
		Disabled = it.Next() != null || !dnc_enabled;

		if (Disabled)
		{
			return;
		}

		Tracker = DayNightTracker.Get();

		if (!dnc_persistent)
		{
			Tracker.HasBeenSet = false;
		}

		if (!Tracker.HasBeenSet)
		{
			Tracker.TimeTicks = int(GetTicksPerDay() * (dnc_starttime <= -1 ? frandom(0, 1.0) : min(1.0, dnc_starttime)));
			Tracker.HasBeenSet = true;
		}

		foreach (s : Level.Sectors)
		{
			if (s.GetTexture(Sector.Ceiling) == skyflatnum || s.GetTexture(Sector.Floor) == skyflatnum)
			{
				Skies.Push(s);
				SkySizes.Push(GetSectorArea(s));
			}

			foreach (l : s.Lines)
			{
				// [Ace] Boom uses a line special to set skies. Get rid of that. We assume full control.
				if (l.special == 190 && l.args[1] == 255)
				{
					TextureID nullTex;
					l.sidedef[0].SetTexture(Side.top, nullTex);
				}
			}
		}

		int iterations = 0;
		int skyCount = Skies.Size();
		foreach (s : Level.Sectors)
		{
			if (SectorsNearSky.Find(s) != SectorsNearSky.Size())
			{
				continue;
			}

			if (Skies.Find(s) != skyCount)
			{
				SectorsNearSky.Push(s);
				OriginalLights.Push(s.lightlevel);
				DistanceFactors.Push(0);
				continue;
			}

			// [Ace] One sector can only be affected by one sky - the closest one.
			double closestSkyDistance = int.Max;
			for (int i = 0; i < skyCount; ++i)
			{
				let sky = Skies[i];
				Vector2 diff = Level.Vec2Diff(s.centerspot, sky.centerspot);
				double dist = diff.LengthSquared() - SkySizes[i];
				if (dist <= 0)
				{
					closestSkyDistance = 0;
					break;
				}
				else if (dist < closestSkyDistance)
				{
					closestSkyDistance = dist;
				}
				//iterations++;
			}

			double distFac = sqrt(closestSkyDistance) / 2048.0;
			if (distFac <= 1.0)
			{
				SectorsNearSky.Push(s);
				OriginalLights.Push(s.lightlevel);
				DistanceFactors.Push(distFac);
			}
		}

		//Console.Printf("%i, %i, %i, %i", Skies.Size(), SectorsNearSky.Size(), Level.Sectors.Size(), iterations);
	}

	override void WorldUnloaded(WorldEvent e)
	{
		if (Disabled)
		{
			return;
		}

		Tracker = DayNightTracker.Get();

		if (dnc_persistent && dnc_advancetime > 0)
		{
			Tracker.AdvanceTime(int(GetTicksPerDay() * min(0.95, dnc_advancetime)));
		}
	}

	override void WorldTick()
	{
		if (Disabled || GameState != GS_LEVEL || Level.MapName ~== "FLAGSHIP")
		{
			return;
		}

		if (!Tracker)
		{
			Tracker = DayNightTracker.Get();
		}

		if (!SyncMusic) { SyncMusic = CVar.GetCVar('dnc_syncmusic', players[consoleplayer]); }

		switch (SyncMusic.GetInt())
		{
			case 0:
			{
				SetMusicVolume(1.0);
				break;
			}
			case 1:
			{
				Volume = SmoothStep(Tracker.GetLightFactor(), 0.125, 0.60);
				SetMusicVolume(Volume);
				break;
			}
			case 2:
			{
				let plr = players[consoleplayer].mo;
				double lightFac = plr.CurSector ? plr.CurSector.lightlevel / 255.0 : 0;
				TargetVolume = SmoothStep(lightFac, 0.125, 0.60);

				if (Volume < TargetVolume)
				{
					Volume = min(TargetVolume, Volume + 0.0025);
				}
				else if (Volume > TargetVolume)
				{
					Volume = max(TargetVolume, Volume - 0.0025);
				}

				SetMusicVolume(Volume);
				break;
			}
		}

		if (Level.Time > 1 && Level.time % TICRATE != 0)
		{
			return;
		}

		Array<string> split;
		FormattedTime.Split(split, ":");

		if (dnc_cycletime > 0)
		{
			Tracker.AdvanceTime(TICRATE);
		}
		else if (split.Size() >= 3)
		{
			Tracker.TimeTicks = (split[0].ToInt() * 3600 + split[1].ToInt() * 60 + split[2].ToInt()) * TICRATE;
		}

		Update();
	}

	override void NetworkProcess(ConsoleEvent e)
	{
		Array<string> split;
		e.Name.Split(split, ":");
		string cmd = e.Name.MakeLower();

		if (cmd.IndexOf("dnc_settime") != -1)
		{
			FormattedTime = split[1];
			FormattedTime.Replace("-", ":");
			return;
		}
	}

	protected void Update()
	{
		TextureID skyTex = TexMan.CheckForTexture("DNSKY"..max(1, int(ceil(SkyPics * Tracker.GetTimeFactor()))), TexMan.Type_Any);
		Level.ChangeSky(skyTex, Level.skytexture2);

		Level.skyspeed1 = 1024.0 / (1000 * GetTicksPerDay() / 35);

		int size = Level.Sectors.Size();
		double fac = Tracker.GetLightFactor();

		for (int i = 0; i < size; ++i)
		{
			Level.Sectors[i].lightlevel = MinLight;
		}

		for (int i = 0; i < SectorsNearSky.Size(); ++i)
		{
			let s = SectorsNearSky[i];
			double distFac = max(0, 1.0 - DistanceFactors[i]) ** 0.45;
			s.lightlevel += int(min(255, OriginalLights[i] * 2) * fac * distFac);
		}

		//Console.Printf("\c[DarkGray]Ticks:\c- %i, \c[DarkGray]TPD:\c- %i, \c[DarkGray]Time (Light) Factor:\c- %.5f (%.5f), \c[DarkGray]Real Time:\c- %s, \c[DarkGray]Sky (curr1, curr2):\c- %s (%s, %s), \c[DarkGray]Volume:\c- %.2f", Tracker.TimeTicks, GetTicksPerDay(), Tracker.GetTimeFactor(), Tracker.GetLightFactor(), FormattedTime, TexMan.GetName(skyTex), TexMan.GetName(Level.skytexture1), TexMan.GetName(Level.skytexture2), Volume);
	}

	const MinLight = 8;
	const SkyPics = 240; // [Ace] Yes, 90% of them are used. Hey, you wanted this, anon. Welcome to GZDoom.

	Array<Sector> Skies;
	Array<double> SkySizes;

	Array<Sector> SectorsNearSky;
	Array<int> OriginalLights;
	Array<double> DistanceFactors;

	String FormattedTime;
	DayNightTracker Tracker;

	protected transient CVar SyncMusic;

	protected double TargetVolume;
	protected double Volume;

	bool Disabled;
}