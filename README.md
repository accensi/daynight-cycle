### Important
---
- Create an `autoexec.cfg` in your GZDoom folder and type `gl_maplightmode 0` in it. This will automatically set the otherwise unsaved CVar to Standard on launch, which is the only light mode that looks good with this mod. Note: it will only do that if the map itself doesn't define a specific light mode.

### Conflicts
---
- Does not work with DarkDoomZ. You can still use the mod for the flashlight, but disable sector lighting modifications. Anything that messes with sector lights in real time is going to be fucky. Nothing I can do about it.
- Also won't work with maps that use skyboxes instead of standard skies.

### Known Issues
---
Boy are there known issues... Where do I even begin?

- Sector detection isn't perfect. The method that did it more accurately resulted in exorbitant iteration counts to the point where Doom levels started having a loading time. Some sectors may look off. Still working on addressing edge cases.
- Light seeping through walls like with dynamic lights. No known fixes at this time. Enjoy it while it's daytime.
- MAY desync if using real time. I have not tested it, but if it does desync, turn that off. And report it if you test that so I can either keep this text or remove it.